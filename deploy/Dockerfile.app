FROM openjdk:8-jdk-alpine
COPY ./runner-*.jar /app/app.jar
ENTRYPOINT java ${JAVA_OPTS:="-Xms512m -Xmx512m"} -XX:+HeapDumpOnOutOfMemoryError -XX:+ExitOnOutOfMemoryError -XX:HeapDumpPath=/app/crash/java.hprof -XX:OnOutOfMemoryError="mv /app/crash/java.hprof /app/crash/heapdump.hprof" -Djava.net.preferIPv4Stack=true -Djava.security.egd=file:/dev/./urandom -jar /app/app.jar